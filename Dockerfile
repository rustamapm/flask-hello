FROM python:3.6
RUN mkdir /app
ADD . /app
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINT FLASK_APP=hello flask run --host=0.0.0.0
